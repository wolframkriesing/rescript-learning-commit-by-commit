# Learning ReScript - Commit by Commit

Learn ReScript - https://rescript-lang.org!

I am also blogging about this here: https://picostitch.com/tag/rescript/

## How to Use This Repo?

* run `docker compose up -d && docker compose exec node bash`   
  which will create, start and enter a docker container with nodejs inside,  
  you can skip this step if you are running nodejs locally
* `npm install` to install all dependencies this project needs
* `npm start` to compile and run the code
* next you might want to go and play around with [hello.res](./src/hello.res)
