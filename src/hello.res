type helloWorld =
  | Hello
  | World
  | WorldCount(int)

type parts = {
  first: string,
  second: string,
}

let renderHello = (h, e) => h ++ e ++ "ello"

module Printer = {
  let convert = (~helloOrWorld) =>
    switch helloOrWorld {
    | Hello => "H"->renderHello("e")
    | World => "World"
    | WorldCount(count) => Js.Int.toString(count)
    }

  let prepare = ((first, space, worldsCount, third)) => {
    {
      first: convert(~helloOrWorld=first) ++ space,
      second: convert(~helloOrWorld=worldsCount) ++ space ++ convert(~helloOrWorld=third),
    }
  }

  let logTwoParts = parts => {
    Js.log(parts.first ++ parts.second)
  }

  (Hello, " ", WorldCount(1), World)->prepare->logTwoParts
}
